package io.eyax.netherrack;


import org.lwjgl.opengl.GL33;

public class Texture {
    private int textureID;

    public Texture()
    {

    }

    public void loadFromFile(String file)
    {
        TextureLoader loader = new TextureLoader();
        loader.loadImage(file);
        textureID = loader.loadTexture();
    }

    public void delete()
    {
        GL33.glDeleteTextures(textureID);
    }

    public void bind()
    {
        GL33.glBindTexture(GL33.GL_TEXTURE0, textureID);
    }

    public int getTextureID()
    {
        return textureID;
    }
}
