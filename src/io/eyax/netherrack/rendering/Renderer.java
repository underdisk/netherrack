package io.eyax.netherrack.rendering;

import org.lwjgl.opengl.GL33;

public class Renderer {
    public void renderModel(Model model)
    {
        GL33.glBindVertexArray(model.getVertexArrayID());

        GL33.glEnableVertexAttribArray(0);
        GL33.glEnableVertexAttribArray(1);
        GL33.glEnableVertexAttribArray(2);

        GL33.glActiveTexture(GL33.GL_TEXTURE0);
        model.getTexture().bind();

        GL33.glDrawElements(GL33.GL_TRIANGLES, model.getVertexCount(), GL33.GL_UNSIGNED_INT, 0);

        GL33.glDisableVertexAttribArray(0);
        GL33.glDisableVertexAttribArray(1);
        GL33.glDisableVertexAttribArray(2);

        GL33.glBindVertexArray(0); //unbind

    }
}
