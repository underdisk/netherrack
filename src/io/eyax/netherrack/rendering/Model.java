package io.eyax.netherrack.rendering;

import io.eyax.netherrack.Texture;
import io.eyax.netherrack.types.Color;
import io.eyax.netherrack.types.Vec2;
import io.eyax.netherrack.types.Vec3;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL33;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

public class Model {
    private int vertexArrayID, vertexBufferID, vertexCount, indexBufferID, colorBufferID, texCoordID;
    private float[] vertices;
    private float[] texCoords;
    private float[] colors;
    private int[] indices;

    private Texture texture = new Texture();

    public Model(Vec3[] vertices, Color[] colors, Vec2[] texCoords, int[] indices)
    {
        this.vertices = normalizeVec3Data(vertices);
        this.texCoords = normalizeVec2Data(texCoords);
        this.colors = normalizeColorData(colors);
        this.indices = indices;
        vertexCount = indices.length;

        createModel();
    }

    public void createModel()
    {
        FloatBuffer buffer = BufferUtils.createFloatBuffer(vertices.length);
        FloatBuffer colorBuffer = BufferUtils.createFloatBuffer(colors.length);
        IntBuffer indicesBuffer = BufferUtils.createIntBuffer(indices.length);
        FloatBuffer texCoordBuffer = BufferUtils.createFloatBuffer(texCoords.length);

        buffer.put(vertices);
        indicesBuffer.put(indices);
        colorBuffer.put(colors);
        texCoordBuffer.put(texCoords);

        buffer.flip(); //GL likes flipped buffers for some reason
        indicesBuffer.flip(); //GL likes flipped buffers for some reason
        colorBuffer.flip(); //GL likes flipped buffers for some reason
        texCoordBuffer.flip();

        vertexArrayID = GL33.glGenVertexArrays(); //Generating Vertex Array
        GL33.glBindVertexArray(vertexArrayID);

        vertexBufferID = GL33.glGenBuffers(); //Generating Vertex Buffer
        GL33.glBindBuffer(GL33.GL_ARRAY_BUFFER, vertexBufferID);
        GL33.glBufferData(GL33.GL_ARRAY_BUFFER, buffer, GL33.GL_STATIC_DRAW); //Setting VAO Data

        indexBufferID = GL33.glGenBuffers();
        GL33.glBindBuffer(GL33.GL_ELEMENT_ARRAY_BUFFER, indexBufferID);
        GL33.glBufferData(GL33.GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL33.GL_STATIC_DRAW);

        colorBufferID = GL33.glGenBuffers();
        GL33.glBindBuffer(GL33.GL_ARRAY_BUFFER, colorBufferID);
        GL33.glBufferData(GL33.GL_ARRAY_BUFFER, colorBuffer, GL33.GL_STATIC_DRAW); //Setting VAO Data

        texCoordID = GL33.glGenBuffers();
        GL33.glBindBuffer(GL33.GL_ARRAY_BUFFER, texCoordID);
        GL33.glBufferData(GL33.GL_ARRAY_BUFFER, texCoordBuffer, GL33.GL_STATIC_DRAW); //Setting VAO Data


        GL33.glBindBuffer(GL33.GL_ARRAY_BUFFER, vertexBufferID);
        GL33.glEnableVertexAttribArray(0);
        GL33.glVertexAttribPointer(0, 3, GL33.GL_FLOAT, false, 0, 0);

        GL33.glBindBuffer(GL33.GL_ARRAY_BUFFER, colorBufferID);
        GL33.glEnableVertexAttribArray(1);
        GL33.glVertexAttribPointer(1, 4, GL33.GL_FLOAT, false, 0, 0);

        GL33.glBindBuffer(GL33.GL_ARRAY_BUFFER, texCoordID);
        GL33.glEnableVertexAttribArray(2);
        GL33.glVertexAttribPointer(2, 2, GL33.GL_FLOAT, false, 0, 0);

        GL33.glBindVertexArray(0); //Unbind

        GL33.glDisableVertexAttribArray(0);
        GL33.glDisableVertexAttribArray(1);
        GL33.glDisableVertexAttribArray(2);

    }

    public void delete()
    {
        GL33.glDeleteVertexArrays(vertexArrayID);
        GL33.glDeleteBuffers(vertexBufferID);
        GL33.glDeleteBuffers(indexBufferID);
        GL33.glDeleteBuffers(colorBufferID);
        GL33.glDeleteBuffers(texCoordID);
    }

    public int getVertexArrayID()
    {
        return vertexArrayID;
    }

    public int getVertexBufferID()
    {
        return vertexBufferID;
    }

    public int getVertexCount()
    {
        return vertexCount;
    }

    public int getIndexBufferID()
    {
        return indexBufferID;
    }

    private float[] normalizeColorData(Color[] colors)
    {
        float[] result = new float[4*colors.length];
        for(int i = 0; i < colors.length; ++i)
        {
            result[4*i] = colors[i].r/255.f;
            result[4*i+1] = colors[i].g/255.f;
            result[4*i+2] = colors[i].b/255.f;
            result[4*i+3] = colors[i].a/255.f;
        }

        return result;
    }

    private float[] normalizeVec3Data(Vec3[] vecs)
    {
        float[] result = new float[3*vecs.length];

        for(int i = 0; i < vecs.length; ++i)
        {
            result[3*i] = vecs[i].x;
            result[3*i+1] = vecs[i].y;
            result[3*i+2] = vecs[i].z;
        }

        return result;
    }

    private float[] normalizeVec2Data(Vec2[] vecs)
    {
        float[] result = new float[2*vecs.length];

        for(int i = 0; i < vecs.length; ++i)
        {
            result[2*i] = vecs[i].x;
            result[2*i+1] = vecs[i].y;
        }

        return result;
    }

    public Texture getTexture()
    {
        return texture;
    }
}
