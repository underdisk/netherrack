package io.eyax.netherrack;

import io.eyax.netherrack.types.Color;
import io.eyax.netherrack.types.Vec2;
import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL33;

import java.nio.DoubleBuffer;

public class Window {

    private int width, height;
    private String title;
    private long window;
    private boolean[] keys = new boolean[GLFW.GLFW_KEY_LAST];
    private boolean[] mouseButtons = new boolean[GLFW.GLFW_MOUSE_BUTTON_LAST];
    private boolean[] gamepadButtons = new boolean[GLFW.GLFW_GAMEPAD_BUTTON_LAST]; //not implemented

    private Color backgroundColor = Color.black();

    public Window(int width, int height, String title)
    {
        this.width = width;
        this.height = height;
        this.title = title;
    }

    public void createWindow()
    {
        if(!GLFW.glfwInit())
        {
            throw new RuntimeException("Could not initialize GLFW");
        }

        GLFW.glfwWindowHint(GLFW.GLFW_VISIBLE, GLFW.GLFW_FALSE);
        GLFW.glfwWindowHint(GLFW.GLFW_RESIZABLE, GLFW.GLFW_FALSE);

        GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_VERSION_MAJOR, 4);
        GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_VERSION_MINOR, 1);

        GLFW.glfwWindowHint(GLFW.GLFW_OPENGL_FORWARD_COMPAT, GLFW.GLFW_TRUE);
        GLFW.glfwWindowHint(GLFW.GLFW_OPENGL_PROFILE, GLFW.GLFW_OPENGL_CORE_PROFILE);

        window = GLFW.glfwCreateWindow(width, height, title, 0, 0);

        if(window == 0)
        {
            throw new RuntimeException("Could not create window");
        }

        GLFWVidMode videoMode = GLFW.glfwGetVideoMode(GLFW.glfwGetPrimaryMonitor());
        GLFW.glfwSetWindowPos(window, (videoMode.width()-width)/2,
                                      (videoMode.height()-height)/2);

        GLFW.glfwMakeContextCurrent(window);
        GL.createCapabilities();



        GLFW.glfwShowWindow(window);
    }

    public boolean isOpen()
    {
        return !GLFW.glfwWindowShouldClose(window);
    }

    public void update()
    {
        for (int i = 0; i < GLFW.GLFW_KEY_LAST; ++i)
        {
            keys[i] = isKeyDown(i);
        }

        for (int i = 0; i < GLFW.GLFW_MOUSE_BUTTON_LAST; ++i)
        {
            mouseButtons[i] = isMouseButtonDown(i);
        }

        GL33.glClearColor(backgroundColor.r/255.f, backgroundColor.g/255.f, backgroundColor.b/255.f, backgroundColor.a/255.f);
        GL33.glClear(GL33.GL_COLOR_BUFFER_BIT);
        GLFW.glfwPollEvents();
    }

    public void swapBuffers()
    {
        GLFW.glfwSwapBuffers(window);
    }

    public boolean isKeyDown(int keycode)
    {
        return GLFW.glfwGetKey(window, keycode) == 1;
    }

    public boolean isKeyPressed(int keycode)
    {
        return isKeyDown(keycode) && !keys[keycode];
    }

    public boolean isKeyReleased(int keycode)
    {
        return !isKeyDown(keycode) && keys[keycode];
    }

    public boolean isMouseButtonDown(int mouseButton)
    {
        return GLFW.glfwGetMouseButton(window, mouseButton) == 1;
    }

    public boolean isMouseButtonPressed(int mouseButton)
    {
        return isMouseButtonDown(mouseButton) && !mouseButtons[mouseButton];
    }

    public boolean isMouseButtonReleased(int mouseButton)
    {
        return !isMouseButtonDown(mouseButton) && mouseButtons[mouseButton];
    }

    public float getMouseX()
    {
        DoubleBuffer buffer = BufferUtils.createDoubleBuffer(1);
        GLFW.glfwGetCursorPos(window, buffer, null);
        return (float)buffer.get(0);
    }

    public float getMouseY()
    {
        DoubleBuffer buffer = BufferUtils.createDoubleBuffer(1);
        GLFW.glfwGetCursorPos(window, null, buffer);
        return (float)buffer.get(0);
    }

    public Vec2 getMousePos()
    {
        return new Vec2(getMouseX(), getMouseY());
    }

    public void stop()
    {
        GLFW.glfwTerminate();
    }

    public void setBackgroundColor(Color color)
    {
        backgroundColor = color;
    }
}
