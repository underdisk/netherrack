package io.eyax.netherrack.types;

public class Color {

    public int r, g, b, a;

    public Color()
    {
        r = g = b = 255;
        a = 255;
    }

    public Color(int r, int g, int b)
    {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = 255;
    }

    public Color(int r, int g, int b, int a)
    {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }

    public static Color black()
    {
        return new Color();
    }

    public static Color white()
    {
        return new Color(255, 255, 255);
    }

    public static Color red()
    {
        return new Color(255, 0, 0);
    }

    public static Color green()
    {
        return new Color(0, 255, 0);
    }

    public static Color blue()
    {
        return new Color(0, 0, 255);
    }


}
