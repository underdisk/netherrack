package io.eyax.netherrack.shaders;

public class BasicShader extends Shader {

    private static final String VERTEX_FILE = "src/io/eyax/netherrack/shaders/basicShader.vsh", FRAGMENT_FILE = "src/io/eyax/netherrack/shaders/basicShader.fsh";

    public BasicShader()
    {
        super(VERTEX_FILE, FRAGMENT_FILE);
    }

    public void bindAllAttributes()
    {
        super.bindAttribute(0, "vertices");
        super.bindAttribute(1, "color");
        super.bindAttribute(2, "texCoord");
    }
}
