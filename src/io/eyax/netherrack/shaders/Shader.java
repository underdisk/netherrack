package io.eyax.netherrack.shaders;

import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL33;
import org.lwjgl.opengl.GL41;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public abstract class Shader {
    private int vertexShaderID, fragmentShaderID, programID;
    private String vertexFile, fragmentFile;

    public Shader(String vertexFile, String fragmentFile)
    {
        this.vertexFile = vertexFile;
        this.fragmentFile = fragmentFile;
    }

    public void createShader()
    {
        programID = GL33.glCreateProgram();

        int tempVAO = GL41.glGenVertexArrays();
        GL41.glBindVertexArray(tempVAO);

        vertexShaderID = GL33.glCreateShader(GL20.GL_VERTEX_SHADER);
        GL33.glShaderSource(vertexShaderID, readFile(vertexFile));
        GL33.glCompileShader(vertexShaderID);

        if(GL33.glGetShaderi(vertexShaderID, GL33.GL_COMPILE_STATUS) == GL33.GL_FALSE)
        {
            System.err.println("Unable to compile Vertex Shader : " + GL33.glGetShaderInfoLog(vertexShaderID));
            throw new RuntimeException("Unable to compile Vertex Shader : " + GL33.glGetShaderInfoLog(vertexShaderID));
        }

        fragmentShaderID = GL33.glCreateShader(GL20.GL_FRAGMENT_SHADER);
        GL33.glShaderSource(fragmentShaderID, readFile(fragmentFile));
        GL33.glCompileShader(fragmentShaderID);

        if(GL33.glGetShaderi(fragmentShaderID, GL33.GL_COMPILE_STATUS) == GL33.GL_FALSE)
        {
            System.err.println("Unable to compile Fragment Shader : " + GL33.glGetShaderInfoLog(fragmentShaderID));
            throw new RuntimeException("Unable to compile Fragment Shader : " + GL33.glGetShaderInfoLog(fragmentShaderID));
        }

        GL33.glAttachShader(programID, vertexShaderID);
        GL33.glAttachShader(programID, fragmentShaderID);

        bindAllAttributes();

        GL33.glLinkProgram(programID);

        if(GL33.glGetProgrami(programID, GL33.GL_LINK_STATUS) == GL33.GL_FALSE)
        {
            System.err.println("Unable to link Shader Program : " + GL33.glGetProgramInfoLog(programID));
            throw new RuntimeException("Unable to link Shader Program : " + GL33.glGetProgramInfoLog(programID));
        }

        GL33.glValidateProgram(programID);

        if(GL33.glGetProgrami(programID, GL33.GL_VALIDATE_STATUS) == GL33.GL_FALSE)
        {
            System.err.println("Unable to validate Shader Program : " + GL33.glGetProgramInfoLog(programID));
            throw new RuntimeException("Unable to validate Shader Program : " + GL33.glGetProgramInfoLog(programID));
        }

        GL41.glBindVertexArray(0);
    }

    public void bind()
    {
        GL33.glUseProgram(programID);
    }

    public void delete()
    {
        GL33.glDetachShader(programID, vertexShaderID);
        GL33.glDetachShader(programID, fragmentShaderID);

        GL33.glDeleteShader(vertexShaderID);
        GL33.glDeleteShader(fragmentShaderID);
        GL33.glDeleteProgram(programID);
    }

    private String readFile(String file)
    {
        BufferedReader reader = null;
        StringBuilder string = new StringBuilder();

        try {
            reader = new BufferedReader(new FileReader(file));
            String line;
            while((line = reader.readLine()) != null)
            {
                string.append(line).append("\n");
            }

        } catch (IOException e)
        {
            System.err.println("File '" + file + "' not found");
        }

        return string.toString();
    }

    public abstract void bindAllAttributes();
    public void bindAttribute(int index, String location)
    {
        GL33.glBindAttribLocation(programID, index, location);
    }
}
