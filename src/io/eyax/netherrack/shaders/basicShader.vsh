#version 400 core

in vec3 vertices;
in vec4 color;
in vec2 texCoord;

out vec4 fColor;
out vec2 fTexCoord;

void main()
{
    fColor = color;
    fTexCoord = texCoord;

    gl_Position = vec4(vertices, 1.0);
}
