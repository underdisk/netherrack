#version 400 core

in vec4 fColor;
in vec2 fTexCoord;

uniform sampler2D sampler;

out vec4 fragColor;

void main()
{
    fragColor = fColor * texture(sampler, fTexCoord);
}
