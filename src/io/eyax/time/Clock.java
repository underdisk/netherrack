package io.eyax.time;

public class Clock {

    private Time time;

    public Clock()
    {
        time = Time.now();
    }

    public Time restart()
    {
        Time res = Time.substract(Time.now(), time);;
        time = Time.now();
        return res;
    }

    public Time getElapsedTime()
    {
        return Time.substract(Time.now(), time);
    }

}
