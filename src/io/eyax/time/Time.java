package io.eyax.time;

public class Time {

    private long nanoSeconds;

    public Time()
    {
        nanoSeconds = 0;
    }

    public static Time add(Time a, Time b)
    {
        return new Time(a.asNanoseconds() + b.asNanoseconds());
    }

    public static Time substract(Time a, Time b)
    {
        return new Time(a.asNanoseconds() - b.asNanoseconds());
    }

    private Time(long nanoSeconds)
    {
        this.nanoSeconds = nanoSeconds;
    }

    public static Time zero()
    {
        return new Time();
    }


    public static Time seconds(double s)
    {
        return new Time((long)s*1000000000);
    }

    public static Time milliseconds(double s)
    {
        return new Time((long)s*1000000);
    }

    public static Time microseconds(double s)
    {
        return new Time((long)s*1000);
    }

    public static Time microseconds(long s)
    {
        return new Time(s);
    }

    public static Time now()
    {
        return new Time(System.nanoTime());
    }

    public long asNanoseconds()
    {
        return nanoSeconds;
    }

    public double asMicroseconds()
    {
        return (double)nanoSeconds / (double)1000.D;
    }

    public double asMilliseconds()
    {
        return (double)nanoSeconds / (double)1000000.D;
    }

    public double asSeconds()
    {
        return (double)nanoSeconds / (double)1000000000.D;
    }
}
