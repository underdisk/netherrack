package main;

import io.eyax.netherrack.*;
import io.eyax.netherrack.rendering.Model;
import io.eyax.netherrack.rendering.Renderer;
import io.eyax.netherrack.shaders.BasicShader;
import io.eyax.netherrack.types.Color;
import io.eyax.netherrack.types.Vec2;
import io.eyax.netherrack.types.Vec3;
import io.eyax.time.Clock;
import io.eyax.time.Time;

public class Main {

    private static Renderer renderer = new Renderer();
    private static BasicShader shader = new BasicShader();

    public static void main(String[] args)
    {
        Window window = new Window(800, 600, "Netherrack Engine");
        window.createWindow();

        Clock clock = new Clock();
        Time deltaTime = new Time();

        window.setBackgroundColor(new Color(0, 128, 255));

        shader.createShader();

        Model model;
        model = new Model(

                new Vec3[]{
                    new Vec3(-0.5f, -0.5f, 0.f),
                    new Vec3(-0.5f, 0.5f, 0.f),
                    new Vec3(0.5f, 0.5f, 0.f),
                    new Vec3(0.5f, -0.5f, 0.f)
                },

                new Color[]
                {
                    Color.red(),
                    Color.green(),
                    Color.blue(),
                    new Color(0, 255, 255)
                },

                new Vec2[]{
                    new Vec2(0, 1),
                    new Vec2(0, 0),
                    new Vec2(1, 0),
                    new Vec2(1, 1)
                },

                new int[]{
                        0, 1, 2,
                        2, 3, 0
                }
        );

        model.getTexture().loadFromFile("stone.png");

        while(window.isOpen())
        {
            if(clock.getElapsedTime().asSeconds() >= 1.f/60.f)
            {
                shader.bind();
                window.update();

                renderer.renderModel(model);

                window.swapBuffers();
                deltaTime = clock.restart();

            }

        }

        shader.delete();
    }
}
